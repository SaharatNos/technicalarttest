﻿ Shader "Unlit/TestUnlitShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1, 1, 1, 1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			Tags {"LightMode"="ForwardBase"}
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			//#pragma multi_compile_instancing
			
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
				//UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				SHADOW_COORDS(1)
				float4 vertex : SV_POSITION;
				float3 lighting : COLOR0;
				float3 ambient : COLOR1;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				//UNITY_SETUP_INSTANCE_ID(v);
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				//Get worldnormal 
				float3 worldNormal = UnityObjectToWorldNormal(v.normal);
				//standard equation for diffuse lighting
				float nl = dot(worldNormal,_WorldSpaceLightPos0.xyz);
				//Get Shperical harmonic by using worldnormal
				o.ambient = ShadeSH9(half4(worldNormal,1));
				o.lighting = nl * _LightColor0;

				TRANSFER_SHADOW(o)

				return o;
			}
			
			fixed4 _Color;
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed shadow = SHADOW_ATTENUATION(i);
				fixed4 col = tex2D(_MainTex, i.uv) * float4(i.lighting,1) * shadow.rrrr + i.ambient.rgbb;
				return col * _Color ;
			}
			ENDCG
		}
		pass
		{
			Tags{ "LightMode" = "ShadowCaster"}
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"

			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f{
				V2F_SHADOW_CASTER;
			};

			v2f vert(appdata v)
			{
				v2f o;

				TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
				return o;
			}
			fixed4 frag(v2f i) : SV_TARGET
			{
				SHADOW_CASTER_FRAGMENT(i);
			}

			ENDCG

		}
	}
}
