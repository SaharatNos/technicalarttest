﻿Shader "Unlit/Ground"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
		    Tags {"LightMode"="ForwardBase"}
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			 
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 pos : POSITION;				
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
			    SHADOW_COORDS(1)
				float4 pos : SV_POSITION;
				float3 lighting : COLOR0;
				float3 ambient : COLOR1;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.pos);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				float3 worldNormal = UnityObjectToWorldNormal(v.normal);			
				float nl = max(0,dot(worldNormal, _WorldSpaceLightPos0.xyz));		

				o.ambient = ShadeSH9(half4(worldNormal,1));	
				o.lighting = nl * _LightColor0;

				TRANSFER_SHADOW(o)

				return o;

			}
			
			fixed4 frag (v2f i) : SV_Target
			{

				fixed shadow = SHADOW_ATTENUATION(i);
				i.lighting *= shadow + i.ambient;
				fixed4 col = tex2D(_MainTex,i.uv) * float4(i.lighting.rgb,1);
				return col;
			}
			ENDCG
		}

        Pass {
            Name "ShadowCaster"
            Tags { "LightMode" = "ShadowCaster" }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_shadowcaster
            #include "UnityCG.cginc"
            
            struct v2f {
                V2F_SHADOW_CASTER;
            };
            
            v2f vert( appdata_base v )
            {
                v2f o;
                TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
                return o;
            }
            
            float4 frag( v2f i ) : SV_Target
            {
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
	}
}
