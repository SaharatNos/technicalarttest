﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LookAndDefined : MonoBehaviour {

	public bool UpdateCheck;
	public string ObjName,ObjNameReplace;
	private GameObject GameObj; 
	public Material ObjMaterial;

	void Start () {
		UpdateCheck = false;
	}
	void Update () 
	{
		//Re-update 
		 if(UpdateCheck == true)
		{
			DetectObj(ObjName,ObjNameReplace,ObjMaterial);
			UpdateCheck = false;
		}
	}
	
	public void DetectObj(string Objectname, string Replacename, Material mat)
	{	
		GameObject Obj = GameObject.Find(Objectname) as GameObject;
		Obj.name = Replacename;
		Obj.GetComponent<Renderer>().material = mat;
	}
}
